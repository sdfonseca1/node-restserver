Este es un pequeño proyecto, donde estoy aprendiendo a crear una API Restfull en NodeJS.

He usado un montón de características como hibernate, Express, Mongoose, entre otros.

Sólo es con fines educativos pero quizá a alguien le pueda interesar.

La base de datos está hecha en MongoDB y tan solo cuenta con una colleción llamada Usuarios.