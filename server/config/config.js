// ============================
//  Puerto
// ============================
process.env.PORT = process.env.PORT || 3000;

// ============================
//  Entorno
// ============================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// ============================
//  Vencimiento del TOKEN
// ============================
// 60 * 60 * 24 * 30
process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30;

// ============================
//  Seed del token
// ============================
// 60 * 60 * 24 * 30
process.env.SEED = process.env.SEED || 'este-es-el-seed-de-desarrollo';

// ============================
//  Base de datos
// ============================
let urlDB;

if(process.env.NODE_ENV === 'dev'){
  urlDB = 'mongodb://localhost:27017/cafe';
}else{
  urlDB = process.env.MONGO_URI;
}

process.env.urlDB = urlDB;


// ============================
//  Google Client ID
// ============================
process.env.CLIENT_ID = process.env.CLIENT_ID || '971624025689-79r4pl0uad1c89nurq7pogte8skld0o9.apps.googleusercontent.com';
