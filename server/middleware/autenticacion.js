const jwt = require('jsonwebtoken');
const usuario = require('../models/usuario');
const { decode } = require('jsonwebtoken');
//=======================
// Verificar token
//=======================
let verificaToken = (req, res, next) =>{

  let token = req.get('token');

  jwt.verify(token, process.env.SEED, (err, decoded) => {
    
    if ( err ){
      return res.status(401).json({
        ok: false,
        err: {
          message: 'Token no válido'
        }
      });
    }

    req.usuario = decoded.usuario;
    next();
  });

  

};

//=======================
// Verificar admin role
//=======================
let verificaAdminRole = (req, res, next) =>{

  let usuario = req.usuario;

  if (usuario.role === 'ADMIN_ROLE'){
    next();
  }else{
    return res.status(401).json({
      ok: false,
      err: {
        message: 'No tienes permisos suficientes para realizar esta acción.'
      }
    });
  }

};


module.exports = {
  verificaToken,
  verificaAdminRole
};